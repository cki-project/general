"""Example unit test."""

import unittest

import responses

from example import __main__


class ExampleTestCase(unittest.TestCase):
    """Example tests."""

    @responses.activate
    def test_example_main(self):
        """Check the behavior of log_ip."""
        ip_address = '1.2.3.4'
        responses.add(responses.GET, 'https://ipv4.icanhazip.com',
                      body=ip_address)
        with self.assertLogs(__main__.LOGGER, 'INFO') as logs:
            __main__.log_ip()
        self.assertIn(ip_address, logs.output[0])
